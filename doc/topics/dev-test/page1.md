# page 1

[Absolute link to topic](topic:dev-test)

[Absolute link to page 2](topic:dev-test/page02)

[Relative link to page 2](page02)

Auto-link to test object type: %testobjtype

Auto-link to test object property: %testobjtype::propc

[Absolute link to non-existing topic](topic:non-existing-topic)

[Absolute link to non-existing page](topic:dev-text/non-existing-page)

[Relative link to non-existing page](non-existing-page)

Auto-link to non-existing object type: %nonexistingobjecttype

Auto-link to non-existing test object property: %testobjtype::nonexistingproperty
