# Source code index
The 'dev' module maintains the sourcecode index and the reference pages on https://my.webhare.dev/reference

## Troubleshooting the dev sourcecode index


If an update to `wh:*` files is not picked up, try

```bash
wh run mod::dev/scripts/tests/dump_consilio_objects.whscr --schedule wh:*
```
