# 0.1.11 - 2022-05-19

## Things that are nice to know
- For some operations that may take some time, a progress indicator is shown in supporting clients
- Added support for showing the VM group id in stack traces

# 0.1.10 - 2022-02-09

## Things that are nice to know
- Add support for retrieving workspace folders


# 0.1.9 - 2021-12-10

## Things that are nice to know
- The `lsp` module is integrated into the `dev` module
- Add support for `showDocument`, to be used by WebHare's `OpenReveal` hook


# 0.1.8 - 2021-07-10

## Things that are nice to know
- Add support for `organizeImports` code action, which cleans up the LOADLIBs of a HareScript source file
- Add support for formatting supported XML files


# 0.1.7 - 2021-05-31

## Things that are nice to know
- Make server debug loglevel configurable
- Move package to `@webhare/language-server`


# 0.1.5 - 2021-05-20

## Things that are nice to know
- Return serverInfo on initialization
- Tweak console logs
- Translate related diagnostic uris


# 0.1.4 - 2021-05-12

## Things that are nice to know
- Improve changed configuration handling


# 0.1.3 - 2021-05-07

## Things that are nice to know
- Add support for incremental text sync
- Make returning documentation issues configurable
- Connection reliability and performance improvements


# 0.1.2 - 2021-04-21

## Things that are nice to know
- Add missing dependency


# 0.1.1 - 2021-04-15

## Things that are nice to know
- Prepare for NPM module publication


# 0.1.0 - 2021-04-15

Initial release

## Things that are nice to know
- Diagnostics for HareScript files
- Add missing LOADLIB code action
