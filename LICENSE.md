Please note the following:
- WebHare is a registered trademark held by WebHare bv
- You do *not* have a license to publish the core documentation for the WebHare platform
  on a public website using the name WebHare.
- The contents of https://www.webhare.dev/ that are not part of the WebHare Platform or dev
  module (ie. all the extra documentation) is *not* available under the MIT license. If you
  received this content in any way or form, please do not publish it on a public website.

We want to avoid any confusing or outdated copies of WebHare documentation 'floating' around on the web.

All other code, content etc in this `dev` module is available under the MIT license:

The MIT License (MIT)

Copyright (c) 1999-2024 WebHare bv

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
