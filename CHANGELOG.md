# August 2022
- Finish `wh dev:push` - you can now do eg `wh dev:push https://webhare.exmaple.net/ @outdated` to update all modules there
- Added `wh dev:reverseproxy` - our 'ngrok'

# December 2021
- Move `lsp` module to `dev`

# September 2021
- Add `wh st` (aka `wh status`) and have it give a better summary of open changes/pushes
