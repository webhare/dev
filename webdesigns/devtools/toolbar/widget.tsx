import * as dompack from '@webhare/dompack';
import { onDomReady } from "@webhare/dompack";
import { getToolsOrigin } from '../support/dtsupport';

export class ToolbarWidget {
  //TODO Make these all private
  toolbar = document.createElement("wh-outputtools");
  toolbarshadow = this.toolbar.attachShadow({ mode: 'open' });

  toolbar_resreload;
  toolbar_resreloadcheck;
  toolbar_assetstatus: HTMLElement;
  toolbar_pagereload;
  toolbar_filestatus: HTMLElement;
  toolbar_resstatus;
  toolbar_pagerepublishreload;
  toolbar_cssreload;
  toolbar_cssreloadcheck;
  toolbar_fullreload;
  toolbar_fullreloadcheck;

  constructor(callbacks: {
    onPageReloadClick: (evt: MouseEvent) => unknown;
    onPageRepublishReloadClick: (evt: MouseEvent) => unknown;
    onCssReloadChange: () => unknown;
    onFullReloadChange: () => unknown;
    onResReloadChange: () => unknown;
  }) {
    this.toolbarshadow.append(
      <>
        <link href={getToolsOrigin() + "/.publisher/sd/dev/devtools/wh-outputtools.css"} rel="stylesheet" />
        {this.toolbar_assetstatus = <wh-outputtool class="wh-outputtool wh-outputtool__assetstatus"></wh-outputtool>}
        {this.toolbar_pagereload = <wh-outputtool class="wh-outputtool wh-outputtool__pagereload" title="Reload after current recompile" onClick={callbacks.onPageReloadClick}>↻</wh-outputtool>}
        {this.toolbar_filestatus = <wh-outputtool class="wh-outputtool wh-outputtool__filestatus"></wh-outputtool>}
        {this.toolbar_resstatus = <wh-outputtool class="wh-outputtool wh-outputtool__resstatus"></wh-outputtool>}
        {this.toolbar_pagerepublishreload = <wh-outputtool class="wh-outputtool wh-outputtool__pagerepublishreload" title="Reload after current recompile" onClick={callbacks.onPageRepublishReloadClick}>↻</wh-outputtool>}
        {this.toolbar_cssreload = <wh-outputtool class="wh-outputtool wh-outputtool__cssreload">
          {this.toolbar_cssreloadcheck = <input id="__wh-outputtool__cssreload" type="checkbox" tabindex="-1" onChange={callbacks.onCssReloadChange} />}
          <label for="__wh-outputtool__cssreload">auto-reload CSS</label></wh-outputtool>
        }
        {this.toolbar_fullreload = <wh-outputtool class="wh-outputtool wh-outputtool__fullreload">
          {this.toolbar_fullreloadcheck = <input id="__wh-outputtool__fullreload" type="checkbox" tabindex="-1" onChange={callbacks.onFullReloadChange} />}
          <label for="__wh-outputtool__fullreload">auto-reload page</label>
        </wh-outputtool>}
        {this.toolbar_resreload = <wh-outputtool class="wh-outputtool wh-outputtool__resreload">
          {this.toolbar_resreloadcheck = <input id="__wh-outputtool__resreload" type="checkbox" tabindex="-1" onChange={callbacks.onResReloadChange} />}
          <label for="__wh-outputtool__resreload">auto-reload resources</label></wh-outputtool>
        }
        <wh-outputtool class="wh-outputtool wh-outputtool__openlive" onClick={this.openLive}><span>open live</span></wh-outputtool>
        <wh-outputtool class="wh-outputtool wh-outputtool__hidetools" onClick={this.onHideToolsClick}><span>hide tools</span></wh-outputtool>
      </>);


    this.toolbar_cssreloadcheck.checked = dompack.getLocal("whoutputtool-cssreload") !== false;
    this.toolbar_fullreloadcheck.checked = dompack.getSession("whoutputtool-fullreload");
    this.toolbar_resreloadcheck.checked = dompack.getLocal("whoutputtool-resreload");

    onDomReady(() => this.showToolbar());
  }

  showToolbar() {
    document.body.append(this.toolbar);
  }

  openLive = () => {
    location.href = '/.dev/actions/openlive.shtml?url=' + encodeURIComponent(location.href);
  };

  onHideToolsClick = () => {
    document.documentElement.classList.remove("wh-outputtool--visible");
    this.toolbar.remove();
  };

}
