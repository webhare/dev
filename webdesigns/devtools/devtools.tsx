//TODO setup explicit devtools bridge types

import "./devtools.scss";
import "./validator";
import "./formprefiller";
import "./openineditor";
import * as dompack from 'dompack';
import * as storage from 'dompack/extra/storage';
import * as bundlewatcher from "./bundlewatcher/bundlewatcher";
import { getToolsSocketPromise, toolssocketdefer } from "./support/connection";
import type { ValidationMessageWithType } from "@mod-platform/js/devsupport/validation";
import { debugFlags } from "@webhare/env";
import { ToolbarWidget } from "./toolbar/widget";
import { getToolsOrigin } from "./support/dtsupport";

declare global {
  interface Window {
    //initialized by debugLoader so we should be able to assume its present
    __loadedDevTools: Set<string>;
  }
}

type WHOutputToolData = { //inferred, get from devbridge
  resources: string[];
};

type RenderingSummary = { //inferred type, link back to a devbridge type
  invokedwitties: Array<{
    component: string;
    data: unknown;
    stacktrace: Array<{
      filename: string;
      line: number;
      col: number;
      func: string;
    }>;
  }>;
};

type AssetPackBundleStatus = { //FIXME get from the devbridge
  messages: ValidationMessageWithType[];
  filedependencies: string[];
  missingdependencies: string[];
  entrypoint: string;
  bundleconfig: {
    extrarequires: string[];
    languages: string[];
    environment: string;
  };
  id: number;
  hasstatus: boolean;
  iscompiling: boolean;
  requirecompile: boolean;
  haserrors: boolean | undefined;
  outputtag: string;
  lastcompile: Date | null;
  isdev: boolean;
  watchcount: number;
  compatibility: string;
};

// let toolbar_resstatus = null, toolbar_resreload = null, toolbar_resreloadcheck = null;
// let toolbar_fullreload = null, toolbar_fullreloadcheck = null;

// eslint-disable-next-line @typescript-eslint/no-explicit-any -- TODO needs typing
let bundlestatus: any = null, filestatus: any = null, whoutputtoolsdata: any = null;
let reloadonok = false;
let hadrecompile = false;
let hadrepublish = false;
let hadresourcechange = false;
const watchedresources = new Array<string>();
let infonode: HTMLElement | null = null;

//Assets we've seen compiling so that we know to reload when they're done (TODO not really robust, we may have missed the recompile if its completion races page load)
const seenCompiling = new Set<string>;

function clearInfoNode() {
  if (infonode) {
    infonode.remove();
    infonode = null;
  }
}
function setInfoNode(data: { wait?: string; errors?: ValidationMessageWithType[] }) {
  if (!infonode) {
    infonode = document.createElement('wh-outputtools-info');
    document.body.appendChild(infonode);
  }

  infonode.classList.toggle("wh-outputtools-info--error", Boolean(data.errors));
  infonode.classList.toggle("wh-outputtools-info--wait", Boolean(data.wait));

  infonode.textContent = '';
  if (data.errors?.length) {
    const errorlist = document.createElement('ul');
    infonode.appendChild(errorlist);
    data.errors.forEach(error =>
      errorlist.append(<li>{formatValidationMessage(error)}</li>)
    );
  } else {
    infonode.textContent = data.wait || '';
  }
}

type SocketBundleStatus = (AssetPackBundleStatus & { getstatuserror: "" }) | ({ getstatuserror: Exclude<string, ""> } & { outputtag: string });

function formatValidationMessage(msg: ValidationMessageWithType): string {
  return `${msg.resourcename}:${msg.line}:${msg.col}: ${msg.type[0].toUpperCase()}${msg.type.substring(1)}: ${msg.message}`;
}

function handleAssetPackStatus(packs: SocketBundleStatus[]) {
  if (packs.some(pack => pack.getstatuserror)) {
    console.warn("Your assetpack has been removed");
    for (const pack of packs) {
      if (pack.getstatuserror) {
        console.log(`${pack.outputtag}: ${pack.getstatuserror}`);
      }
    }
    bundlestatus = null;
    updateToolbar();
    return;
  }

  //NOTE: If we get here, no packs have a non-empty getstatus. activePacks is there to keep TS happy
  const activePacks = packs as AssetPackBundleStatus[];
  const doneCompiling = [...seenCompiling].filter(outputtag => !activePacks.some(pack => pack.outputtag === outputtag && pack.iscompiling));
  doneCompiling.forEach(outputtag => seenCompiling.delete(outputtag));
  activePacks.filter(pack => pack.iscompiling).forEach(pack => seenCompiling.add(pack.outputtag));
  const anyCompiling = activePacks.some(pack => pack.iscompiling);
  const anyErrors = activePacks.some(pack => !pack.iscompiling && pack.messages.some(msg => msg.type === "error"));
  if (anyCompiling) {
    console.warn("Your assets are out of date and are being recompiled");
    if (debugFlags.devdebug)
      console.log(`[dev/debugjs] Currently compiling: ${activePacks.filter(pack => pack.iscompiling).map(pack => pack.outputtag).join(", ")}`);
    hadrecompile = true;
    if (toolbarWidget.toolbar_fullreloadcheck.checked && !reloadonok) {
      toolbarWidget.toolbar_pagereload.classList.add("wh-outputtool__pagereload-scheduled");
      reloadonok = true;
    }
    setInfoNode({ wait: "Your assets are out of date and are being recompiled" });
  } else if (anyErrors) {
    console.error("Your assets are out of date because of a compilation error");

    const allerrors = [];
    for (const pack of activePacks)
      for (const msg of pack.messages) {
        if (msg.type === "error")
          allerrors.push(msg);

        console.log(`(${pack.outputtag}: ${formatValidationMessage(msg)}`);
      }

    setInfoNode({ errors: allerrors });
  } else {
    if (debugFlags.devdebug)
      console.log(`[dev/debugjs] Currently not compiling`);
    clearInfoNode();
  }

  bundlestatus = { iscompiling: anyCompiling, isok: !anyErrors };
  updateToolbar();

  checkReload();

  // if (!msgdata.iscompiling && msgdata.haserrors) {
  //   realerrors = msgdata.info.errors;
  //   for (i = 0; i < realerrors.length; ++i) {
  //     if (realerrors[i].resource) {
  //       let errloc = "Error involving " + realerrors[i].resource;
  //       if (realerrors[i].line)
  //         errloc += " line " + realerrors[i].line;
  //       if (realerrors[i].col)
  //         errloc += " column " + realerrors[i].col;

  //       console.log(errloc);
  //     }
  //     console.log(realerrors[i].message);
  //   }
  // }

  for (const pack of doneCompiling)
    onCompilingDone(pack);
}


function handleToolssocketMessage(event: MessageEvent) {
  const msgdata = JSON.parse(event.data);
  let i, realerrors;

  if (msgdata.type === "greeting")
    return;

  if (msgdata.type === "assetpacks") {
    handleAssetPackStatus(msgdata.packs);
    return;
  }
  if (msgdata.type === "file") {
    if (msgdata.ispreview)
      return;

    if (!msgdata.hasfile) {
      console.warn("This page is not associated with a file");
    } else if (msgdata.isdeleted) {
      console.warn("This file has been deleted");
    } else if (msgdata.ispublishing) {
      console.warn("This file is being republished");
      hadrepublish = true;

      if (toolbarWidget.toolbar_fullreloadcheck.checked && !reloadonok) {
        toolbarWidget.toolbar_pagerepublishreload.classList.add("wh-outputtool__pagerepublishreload-scheduled");
        reloadonok = true;
      }
    } else if (msgdata.haserrors) {
      console.error("Republishing this file failed with errors");
      console.log(msgdata.message);
    } else if (msgdata.haswarnings) {
      console.error("Republishing this file completed with warnings");
      console.log(msgdata.message);
    }

    const publishingdone = !msgdata.ispublishing && bundlestatus && bundlestatus.ispublishing && !msgdata.haserrors;
    filestatus = { hasfile: msgdata.hasfile, isdeleted: msgdata.isdeleted, ispublishing: msgdata.ispublishing, isok: !msgdata.haserrors, haswarnings: msgdata.haswarnings };
    updateToolbar();

    checkReload();

    if (publishingdone && msgdata.haserrors) {
      realerrors = msgdata.info.errors;
      for (i = 0; i < realerrors.length; ++i)
        console.log(realerrors[i].message);
    }
    return;
  }
  if (msgdata.type === "resource-change") {
    hadresourcechange = true;
    if (toolbarWidget.toolbar_resreloadcheck.checked)
      reloadonok = true;
    updateToolbar();
    checkReload();
    return;
  }
  console.error("Unexpected message of type '" + msgdata.type + "'", event);
}

function setupWebsocket() {
  let toolssocket;
  try {
    toolssocket = new WebSocket('ws' + getToolsOrigin().substring(4) + "/.dev/tools.whsock?source=" + encodeURIComponent(location.origin + location.pathname));
  } catch (e) {
    if (debugFlags.devdebug)
      console.error("[dev/debugjs] unable to set up websocket", e);
    return;
  }

  toolssocket.addEventListener('open', () => toolssocketdefer.resolve(toolssocket));
  toolssocket.addEventListener('message', handleToolssocketMessage);
}

function checkReload() {
  if (!reloadonok)
    return;

  const bundle_done = !bundlestatus || (!bundlestatus.iscompiling);
  const file_done = !filestatus || (!filestatus.deleted && !filestatus.ispublishing);
  if (bundle_done && file_done) {
    const bundle_ok = !bundlestatus || bundlestatus.isok;
    const file_ok = !filestatus || filestatus.isok;

    if (bundle_ok && file_ok) {
      /* we used to attempt to reload the parent iframe, that might have been useful with frontend tests? but is very annoying when developing iframed components */
      reloadonok = false;
      window.location.reload();
      console.log("Reloading scheduled");
    } else {
      console.log("Compilation/publishing failed, cancelling reload");
      reloadonok = false;
      toolbarWidget.toolbar_pagereload.classList.remove("wh-outputtool__pagereload-scheduled");
      toolbarWidget.toolbar_pagerepublishreload.classList.remove("wh-outputtool__pagerepublishreload-scheduled");
    }
  }
}

function onCompilingDone(bundletag: string) {
  if (debugFlags.devdebug)
    console.log(`[dev/debugjs] Compilation of ${bundletag} completed`);
  if (toolbarWidget.toolbar_cssreloadcheck.checked)
    void bundlewatcher.reloadCSSForBundle(bundletag);
}

function onCssReloadChange() {
  if (toolbarWidget.toolbar_cssreloadcheck.checked)
    storage.setLocal("whoutputtool-cssreload", null);
  else
    storage.setLocal("whoutputtool-cssreload", false);
}

function onFullReloadChange() {
  if (toolbarWidget.toolbar_fullreloadcheck.checked) {
    storage.setSession("whoutputtool-fullreload", true);
    if (hadrecompile || hadrepublish) {
      reloadonok = true;
      checkReload();
    }
  } else
    storage.setSession("whoutputtool-fullreload", null);
}

function onResReloadChange() {
  if (toolbarWidget.toolbar_resreloadcheck.checked) {
    storage.setSession("whoutputtool-resreload", true);
    if (hadresourcechange) {
      reloadonok = true;
      checkReload();
    }
  } else
    storage.setSession("whoutputtool-resreload", null);
}

async function onPageReloadClick(e: MouseEvent) {
  console.log("onPageReloadClick", e);
  e.preventDefault();
  e.stopPropagation();
  if (!bundlestatus)
    return;

  const livesocket = await getToolsSocketPromise();
  toolbarWidget.toolbar_pagereload.classList.add("wh-outputtool__pagereload-scheduled");
  reloadonok = true;
  if (!bundlestatus || (!bundlestatus.iscompiling && (!hadrecompile || !bundlestatus.isok))) {
    livesocket.send(JSON.stringify({ type: 'recompileassetpack', uuids: bundlewatcher.getAllBundleIds() }));
    bundlestatus.iscompiling = true;
    updateToolbar();
  }

  // Also republish if republishing had failed
  if (!filestatus || (!filestatus.isrepublishing && !filestatus.isok)) {
    livesocket.send(JSON.stringify({ type: 'republishfile', url: window.location.href }));
    if (filestatus)
      filestatus.ispublishing = true;
    updateToolbar();
  }

  checkReload();
}

async function onPageRepublishReloadClick(e: MouseEvent) {
  console.log("onPageRepublishReloadClick", e);
  e.preventDefault();
  e.stopPropagation();
  if (!filestatus)
    return;

  const livesocket = await getToolsSocketPromise();
  toolbarWidget.toolbar_pagerepublishreload.classList.add("wh-outputtool__pagerepublishreload-scheduled");
  reloadonok = true;
  if (!filestatus || (!filestatus.isrepublishing && (!hadrepublish || !filestatus.isok))) {
    livesocket.send(JSON.stringify({ type: 'republishfile', url: window.location.href }));
    filestatus.ispublishing = true;
    updateToolbar();
  }

  // Also recompile if bundle compile failed
  if (!bundlestatus || (!bundlestatus.iscompiling && !bundlestatus.isok)) {
    livesocket.send(JSON.stringify({ type: 'recompileassetpack', uuids: bundlewatcher.getAllBundleIds() }));
    bundlestatus.iscompiling = true;
    updateToolbar();
  }

  checkReload();
}

function onDomReady() {
  document.documentElement.classList.add("wh-outputtool--active");
  document.documentElement.classList.add("wh-outputtool--visible");
  window.addEventListener("wh:outputtools-extradata", function (evt: Event) { processResourceData((evt as CustomEvent).detail); });

  updateToolbar();

  const renderingsummarynode = document.getElementById("wh-rendering-summary");
  if (renderingsummarynode) {
    const renderingsummary = JSON.parse(renderingsummarynode.textContent!) as RenderingSummary;
    if (renderingsummary.invokedwitties)
      renderingsummary.invokedwitties.forEach(function (witty, idx) {
        console.groupCollapsed("Witty #" + (idx + 1) + ": " + witty.component);
        console.log(witty.data);
        console.groupCollapsed("Stacktrace");
        witty.stacktrace.forEach(trace => {
          console.log(trace.filename + ":" + trace.line + ":" + trace.col + " " + trace.func);
        });
        console.groupEnd();
        console.groupEnd();
      });
  }

  whoutputtoolsdata = document.getElementById("wh-outputtoolsdata");
  if (whoutputtoolsdata) {
    whoutputtoolsdata = JSON.parse(whoutputtoolsdata.textContent);
    if (whoutputtoolsdata)
      processResourceData(whoutputtoolsdata as WHOutputToolData);
  }
}

async function processResourceData(data: WHOutputToolData) {
  if (data.resources) {
    for (let i = 0; i < data.resources.length; ++i)
      if (watchedresources.indexOf(data.resources[i]) === -1)
        watchedresources.push(data.resources[i]);
  }

  const livesocket = await getToolsSocketPromise();
  livesocket.send(JSON.stringify({ type: 'watchresources', resources: watchedresources }));
}

function updateToolbar() {
  if (!toolbar)
    return;

  const assetsstatus = bundlestatus ? bundlestatus.iscompiling ? "compiling" : bundlestatus.isok ? (hadrecompile ? "outdated" : "OK") : "ERRORS" : "unknown";
  toolbarWidget.toolbar_assetstatus.textContent = assetsstatus;

  let className = "wh-outputtool__assetstatus-" + assetsstatus.toLowerCase();
  let classNames = toolbarWidget.toolbar_assetstatus.className.split(" ");
  if (classNames.indexOf(className) < 0) {
    classNames = classNames.filter(function (cls) {
      return cls.indexOf("wh-outputtool__assetstatus-") !== 0;
    });
    classNames.push(className);
    toolbarWidget.toolbar_assetstatus.className = classNames.join(" ");
  }

  const showfilestatus = filestatus
    ? filestatus.hasfile
      ? filestatus.isdeleted
        ? "deleted"
        : filestatus.ispublishing
          ? "publishing"
          : filestatus.isok
            ? (hadrepublish
              ? "outdated"
              : filestatus.haswarnings
                ? "warnings"
                : "OK")
            : "ERRORS"
      : "na"
    : "unknown";
  toolbarWidget.toolbar_filestatus.textContent = showfilestatus;
  toolbarWidget.toolbar_filestatus.style.display = filestatus && !filestatus.ispreview ? '' : 'none';
  toolbarWidget.toolbar_pagerepublishreload.style.display = filestatus && !filestatus.ispreview ? '' : 'none';

  className = "wh-outputtool__filestatus-" + showfilestatus.toLowerCase();
  classNames = toolbarWidget.toolbar_filestatus.className.split(" ");
  if (classNames.indexOf(className) < 0) {
    classNames = classNames.filter(cls => {
      return cls.indexOf("wh-outputtool__filestatus-") !== 0;
    });
    classNames.push(className);
    toolbarWidget.toolbar_filestatus.className = classNames.join(" ");
  }

  const showresstatus = hadresourcechange ? "modified" : "OK";
  toolbarWidget.toolbar_resstatus.textContent = showresstatus;
  toolbarWidget.toolbar_resstatus.style.display = whoutputtoolsdata && whoutputtoolsdata.resources ? '' : 'none';
  toolbarWidget.toolbar_resstatus.className = "wh-outputtool wh-outputtool__resstatus wh-outputtool__resstatus-" + (hadresourcechange ? "modified" : "ok");

  window.__loadedDevTools.add("dev:devtools");
}

function initForFile(toolssocket: WebSocket) {
  toolssocket.send(JSON.stringify({ type: 'watchurl', url: window.location.href }));
  if (watchedresources.length)
    toolssocket.send(JSON.stringify({ type: 'watchresources', resources: watchedresources }));
}

///////////////////////////////////////////////////////////////////////////
//
// Init
//

const toolbarWidget = new ToolbarWidget({
  onPageReloadClick,
  onCssReloadChange,
  onFullReloadChange,
  onPageRepublishReloadClick,
  onResReloadChange
});

dompack.onDomReady(onDomReady);
setupWebsocket();
getToolsSocketPromise().then(socket => initForFile(socket));

window.whDev = {
  watchAssetPack: bundlewatcher.watchAssetPack
};
