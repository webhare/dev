// Prism downloaded from https://prismjs.com/download.html#themes=prism-okaidia&languages=markup+css+clike+javascript+scss&plugins=line-numbers
// hotfixed to set 'manual' to true
import "./prism.js";
import "./prism.css";

import "./harescript.es";
import "./witty.es";

import * as dompack from 'dompack';

//TODO remove manual mode above, and just render proper output from harescript ?
dompack.register('code', node =>
{
  if(!node.className.includes('language-'))
    return; //this is not a block level <code>

  let wrapper = dompack.closest(node, 'pre');
  if (!wrapper)
  {
    //wrap inside a <pre>
    wrapper = document.createElement('pre');
    wrapper.classList.add("line-numbers");
    //replace softbreaks with plain linefeeds
    dompack.qSA(node, 'br').forEach(el => el.parentNode.replaceChild(document.createTextNode('\n'), el) );
    node.parentNode.insertBefore(wrapper, node);
    wrapper.appendChild(node);
  }
  Prism.highlightAllUnder(wrapper);
});
