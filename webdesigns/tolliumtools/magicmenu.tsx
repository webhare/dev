import * as dompack from "@webhare/dompack";
import type * as debuginterface from "@mod-tollium/js/internal/debuginterface";

export function addToMagicMenu(comp: debuginterface.ToddCompBase, submenu: HTMLUListElement): void {
  const addactions: HTMLLIElement[] = [];
  const isbackendapp = "queueEventNoLock" in comp.owner.hostapp;

  addactions.push(<li onClick={() => logElement(comp)}>Log {comp.name} to console</li>);
  if (isbackendapp)
    addactions.push(<li onClick={() => editElement(comp)}>Edit element {comp.name}</li>);

  submenu.append(<li class="divider" />, ...addactions);
}

function logElement(comp: debuginterface.ToddCompBase) {
  console.log("logElement %s: %o", comp.name, comp);
}

function editElement(comp: debuginterface.ToddCompBase) {
  const backendapp = comp.owner.hostapp as debuginterface.BackendApplication;

  const componentpath = [];
  for (let goingup: debuginterface.ToddCompBase | null = comp; goingup; goingup = goingup.parentcomp)
    componentpath.push(goingup.name);

  backendapp.queueEventNoLock("$devhook", { action: "openineditor", screen: comp.owner.node.dataset.tolliumscreen, componentpath });
}
