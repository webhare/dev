/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

/* globals Rainbow */
import * as dompack from 'dompack';

require("rainbow/js/rainbow.js");
require("rainbow/js/language/css.js");
require("rainbow/js/language/javascript.js");
require("rainbow/js/language/html.js");
require("rainbow/themes/github.css");

require("./harescript-rainbow").register(Rainbow);
require("./xml-rainbow").register(Rainbow);

dompack.onDomReady(() => Rainbow.color());
