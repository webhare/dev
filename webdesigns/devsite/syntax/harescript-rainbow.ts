/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

function registerHareScript(Rainbow) {
  Rainbow.extend('harescript', [
    {
      'name': 'comment',
      'pattern': /\/\*[\s\S]*?\*\/|(\/\/)[\s\S]*?$/gm
    },
    {
      'matches': {
        1: [
          {
            'name': 'keyword.operator',
            'pattern': /\=|\+/g
          },
          {
            'name': 'keyword.dot',
            'pattern': /\./g
          }
        ],
        2: {
          'name': 'string',
          'matches': {
            'name': 'constant.character.escape',
            'pattern': /\\('|"|`){1}/g
          }
        }
      },
      'pattern': /(\(|\s|\[|\=|:|\+|\.|\{)(('|"|`)([^\\\1]|\\.)*?(\3))/gm
    },
    {
      'name': 'comment',
      'pattern': /\/\*[\s\S]*?\*\//gm
    },
    {
      'name': 'constant.numeric',
      'pattern': /\b(\d+(\.\d+)?(e(\+|\-)?\d+)?(f|d|i64)?|0x[\da-f]+)\b/gi
    },
    {
      'matches': {
        1: 'vartype'
      },
      'pattern': /\b(ARRAY|BLOB|BOOLEAN|DATETIME|FIXEDPOINT|FLOAT|FUNCTION PTR|INTEGER|INTEGER64|MACRO PTR|MONEY|OBJECT|OBJECTTYPE|RECORD|REF|SCHEMA|STRING|TABLE|VARIANT|WEAKOBJECT)(?=\(|\b)/gi
    },
    {
      'matches': {
        1: 'keyword'
      },
      'pattern': /\b(DELETE|FUNCTION|INSERT|MACRO|SELECT|ALL|AS|ASC|AT|BY|CELL|COLUMN|CLASSTYPE|CROSS|DEFAULTSTO|DESC|DISTINCT|END|EXCEPT|EXPORT|EXTEND|EXTENDSFROM|FALSE|FROM|FULL|GROUP|HAVING|INDEX|INNER|INTERSECT|INTO|JOIN|KEY|LIMIT|MEMBER|NEW|NULL|NVL|OFFSET|ONLY|ORDER|OUTER|PROPERTY|SET|TEMPORARY|TRUE|UNIQUE|USING|VALUES|VAR|VARTYPE|WHERE|LOADLIB)(?=\(|\b)/gi
    },
    {
      'matches': {
        1: 'keyword.attribute'
      },
      'pattern': /\b(CONSTANT|DEINITMACRO|DEPRECATED|EXECUTESHARESCRIPT|EXTERNAL|INTERNALPROTECTED|ISCOUNT|NOSTATEMODIFY|READONLY|SKIPTRACE|SPECIAL|TERMINATES|VARARG)(?=\(|\b)/gi
    },
    {
      'matches': {
        1: 'keyword.control'
      },
      'pattern': /\b(AWAIT|BREAK|CASE|CATCH|CONTINUE|ELSE|FINALLY|FOR|FOREVERY|IF|GOTO|RETURN|SWITCH|THROW|TRY|WHILE|YIELD)(?=\(|\b)/gi
    },
    {
      'matches': {
        1: 'keyword.modifier'
      },
      'pattern': /\b(AGGREGATE|ASYNC|CONSTANT|DEFAULT|PRIVATE|PTR|PUBLIC|STATIC|UPDATE|__ATTRIBUTES__)(?=\(|\b)/gi
    },
    {
      'matches': {
        1: 'keyword.operator'
      },
      'pattern': /\b(AND|BITAND|BITLSHIFT|BITNEG|BITOR|BITRSHIFT|BITXOR|CONCAT|IN|LIKE|NOT|OR|TYPEID|XOR)(?=\(|\b)/gi
    },
    {
      'name': 'keyword.operator',
      'pattern': /\+|:=|\!|\-|&(gt|lt|amp);|\||\*|\=|\^/g
    },
    {
      'matches': {
        1: 'function.call.entity'
      },
      'pattern': /(\w+?)(?=\()/g
    },
    {
      'matches': {
        2: 'entity.name.function'
      },
      'pattern': /(\w|\*) +((\w+)(?= ?\())/g
    }
  ]);

  /* Not used
      {
          'matches': {
              1: 'constant.numeric',
              2: 'keyword.unit'
          },
          'pattern': /(\d+)(px|cm|s|%)?/g
      },
      {
          'matches': {
              1: 'storage.function',
              2: 'entity.name.function'
          },
          'pattern': /(function)\s(.*?)(?=\()/g
      },
      {
          'matches': {
              1: 'storage.type',
              3: 'storage.type',
              4: 'entity.name.function'
          },
          'pattern': /\b((un)?signed|const)? ?(void|char|short|int|long|float|double)\*? +((\w+)(?= ?\())?/g
      },
  */

  Rainbow.extend('html', [
    {
      'name': 'source.harescript.embedded',
      'matches': {
        2: {
          'language': 'harescript'
        }
      },
      'pattern': /<\?wh([\s\S]*?)(\?>)/gm
    }
  ], true);
}

module.exports = { register: registerHareScript };
