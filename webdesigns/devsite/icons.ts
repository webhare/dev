/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

import * as dompack from 'dompack';
import * as toddImages from "@mod-tollium/js/icons";
import './icons.scss';

function initicons() {
  dompack.qSA("img.ixo,img.svg").forEach((node, index, all) => {
    const img = toddImages.createImage(node.getAttribute("data-fqn"), parseInt(node.getAttribute("data-size")), parseInt(node.getAttribute("data-size")), node.getAttribute("data-color"));
    img.setAttribute("id", node.getAttribute("id"));
    img.setAttribute("title", node.getAttribute("title"));
    img.setAttribute("alt", node.getAttribute("alt"));
    img.setAttribute("class", node.getAttribute("class"));
    img.setAttribute("data-fqn", node.getAttribute("data-fqn"));
    img.setAttribute("data-size", node.getAttribute("data-size"));
    img.setAttribute("data-color", node.getAttribute("data-color"));
    node.parentNode.replaceChild(img, node);
  });

  dompack.qSA("img.nocheck").forEach((node, index, all) => {
    const img = toddImages.createImage(node.getAttribute("data-fqn"), parseInt(node.getAttribute("data-size")), parseInt(node.getAttribute("data-size")), node.getAttribute("data-color"));
    img.setAttribute("id", node.getAttribute("id"));
    img.setAttribute("title", node.getAttribute("title"));
    img.setAttribute("alt", node.getAttribute("alt"));
    img.setAttribute("class", node.getAttribute("class"));
    img.setAttribute("data-fqn", node.getAttribute("data-fqn"));
    img.setAttribute("data-size", node.getAttribute("data-size"));
    img.setAttribute("data-color", node.getAttribute("data-color"));
    node.parentNode.replaceChild(img, node);
  });
}

dompack.onDomReady(initicons);

