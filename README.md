# WebHare Dev module

The new WebHare Dev module. Copyright WebHare bv 1999-2021. MIT Licensed

To install, `wh module get git@gitlab.com:webhare/dev.git` or `wh module get https://gitlab.com/webhare/dev.git`

You should only install this module on development servers, never in production!

## WH Extensions
- `wh up` - Update all modules
- `wh umic` - Update all modules and make+install+start WebHare (like `wh mic`)

## Development tools
The development tools are normally inserted by the webdesign. If you want to
manually enable the devtools on a page, insert `<script src="/.wh/dev/debug.mjs" type="module"></script>`.
It will automatically watch any assetpack included through a `<script>` or `<link>`
tag in the `<head>`, even if they're dynamically updated

## Troubleshooting
If a rewrite isn't working properly, try
```bash
wh dev:rewrite --debug <infile> /tmp/outfile
```
and watch the messages

Note that xsd files are only rewritten if:
- They appear to be contain form components/handlers
- They appear to be contain tollium components
- They explicitly set their format to a rewritable XMLSchema using `xmlns:rewrite="http://www.webhare.net/xmlns/dev/rewrite" rewrite:format="http://www.w3.org/2001/XMLSchema#schema"`

## LSP
The Dev module includes an [Language Server Protocol](https://microsoft.github.io/language-server-protocol/specifications/specification-current/)
server implementation which can be used in editors using the [WebHare language server](https://www.npmjs.com/package/@webhare/language-server)
package.

### Features
The following LSP features are supported by the WebHare language server:

* File diagnostics for HareScript and Witty files and supported XML files (e.g. module definitions, WRD schemas, screens,
 site profiles)
* Code actions to automatically add missing LOADLIBs and remove unused LOADLIBs
* Documentation popups on hover
* Jump to definition


### Installation and usage
The WebHare language server is supported in the following applications:

* Sublime Text, through the [`LSP-webhare`](https://gitlab.com/webhare/lsp/LSP-webhare) package
* Visual Studio Code, through the [`webhare-language-vscode`](https://gitlab.com/webhare/lsp/webhare-language-vscode) extension
* Nova, through the [`WebHare`](https://extensions.panic.com/extensions/dev.webhare/dev.webhare.WebHare/) extension

See those repositories for specific installation and configuration instructions.


### How it works
* The language clients (eg the VS Code WebHare extension or the Sublime LSP-WebHare package) invoke `wh dev:languageserver` (usually through runkt).
* The language server is started (the main script `server.ts` is run).
* The server sets up a connection to the editor (in `connection.ts`) which receives the application capabilities and
  configuration and sends the language server capabilities back.
* The language server actions are invoked through events on the connection object (e.g. `connection.onDefinition`), which in
  turn call the appropriate functions in `service.ts` (e.g. `definitionRequest`).
* The functions in `service.ts` call the `sendRequest` (expecting a result) or `sendNotification` (no result expected)
  function of the WebHareConnection.
* The module uses the `HareScriptFile` object as an abstraction for a HareScript file, which has the actual implementation of
  the different file actions.
* Code action requests return commands, which are registered through the server capabilities. When a command is run by the
  editor, the requested command is run by dynamically calling the function that implements the command in `service.whlib`
  (prefixed with `CMD_`). The first argument of a called command is always the text document uri, followed by the arguments
  returned with the code action.
* The language server also defines custom actions, which are prefixed with `webHare/`, for example `webHare/getStackTrace`.
  These are defined in `protocol.ts`. The exported definitions of `protocol.ts` can be used in language clients that are
  written in TypeScript.


### Development

When working on custom LSP commands, the type definitions might have to be updated. These are located in a separate NPM
package. For local development, this package can be linked locally:

* Check out the [`@webhare/lsp-types`](https://gitlab.webhare.com/webhare-opensource/lsp/webhare-lsp-types) NPM package
* Within the package directory, run `npm link` to make it available for linking locally
* Within the `dev` module, run `npm link @webhare/lsp-types` to link it locally
* Within the LSP client package, run `npm link @webhare/lsp-types` as well


## MacOS: adding 'open module' as service in your 'Services' menu
Whith these instructions, you can add a menu item to you 'Services' menu that will prompt you for a module to edit,
and opens an IDE with all relevant folders for that module (the module itself and the dependecies).
For VS code, by default a persistent workspace is created per module.
* Start 'Automator' from Launchpad
* Choose 'Quick Action'
* In Library > Utilities, double-click on 'Run Shell Script'
* Add the following line to the script:
  ```
  $HOME/projects/webhare/whtree/bin/wh dev:editmodule
  ```
  This script will create a workspace for the chosen
  For VS code
  Add `-n` to the line when you want a fresh workspace (and a new window). This enables you to open multiple windows
  with the same module.
* Save the action ('Save as...' in the menu and choose a name)
* Add a shortcut to the service by opening 'Preferences', choose 'Keyboard', select tab 'Shortcuts'. In the left list,
  choose 'Services' and look for the newly added service within node 'General'

Existing automator services can be edited by opening them from the `~/Library/Services` folder.

## Deployment
The `master` and `release/n.nn` branches upload a version of the dev module to https://build.webhare.dev/ci/devmodule/dev-MAJOR.MINOR.whmodule
eg https://build.webhare.dev/ci/devmodule/dev-5.2.whmodule
