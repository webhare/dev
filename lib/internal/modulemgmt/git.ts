import { logDebug } from "@webhare/services/src/logging";
import * as child_process from "child_process";
import * as git from 'isomorphic-git';
import simpleGit from 'simple-git';
import fs from 'node:fs/promises';
import path from 'path';

//TODO is this useful?  https://isomorphic-git.org/docs/en/snippets#use-native-git-credential-manager

export async function executeGitCommand(parameters: string[], { workingdir = "" } = {}) {
  // Launch a git subprocess with the specified parameters, capturing its output
  let output = '', errors = '';
  const proc = child_process.spawn("git", parameters, {
    cwd: workingdir,
    stdio: ["ignore", "pipe", "pipe"]
  });
  proc.stdout.on("data", data => output += data);
  proc.stderr.on("data", data => errors += data);

  const exitinfo = await new Promise<number | string>(resolve =>
    proc.on("exit", (code: number, signal: string) => resolve(signal || code)));

  logDebug("dev:executegitcommand", { exitinfo, output, errors });
  return { exitcode: exitinfo, output: output + '\n' + errors };
}

export async function executeGitCommandForeground(parameters: string[], { workingdir = "" } = {}) {
  const proc = child_process.spawn("git", parameters, {
    cwd: workingdir,
    stdio: ["inherit", "inherit", "inherit"]
  });

  const exitinfo = await new Promise<number | string>(resolve =>
    proc.on("exit", (code: number, signal: string) => resolve(signal || code)));

  return exitinfo;
}


function tryOrFallback<T>(func: () => Promise<T>, fallback: T): Promise<T> {
  return func().catch(() => fallback);
}

export async function getRepoInfo(dir: string) {
  const gitdir = (await fs.stat(path.join(dir, ".git"))).isDirectory() ?
    path.join(dir, ".git") :
    (await fs.readFile(path.join(dir, ".git"), "utf8")).trim().substring(8);

  const branch = await git.currentBranch({ fs, dir, gitdir }) || 'HEAD';
  const head_oid = await git.resolveRef({ fs, dir, gitdir, ref: branch });
  const origin_oid = await tryOrFallback(() => git.resolveRef({ fs, dir, gitdir, ref: "origin/" + branch }), '');
  const remote_url = (await git.listRemotes({ fs, dir, gitdir })).filter(r => r.remote === 'origin')[0]?.url ?? "";
  const logentries = await git.log({ fs, dir, gitdir, depth: 100 });

  const gitty = simpleGit({
    baseDir: dir
  });

  const status = await gitty.status();
  // if (!status.isClean()) console.error(status);
  const paths = status.files.map(f => ({ path: f.path }));

  const result = {
    branch,
    head_oid,
    msg: "", //??
    origin_oid,
    remote_url,
    status: "ok",
    paths,
    commits: logentries.map(logentry =>
    ({
      date: new Date(logentry.commit.author.timestamp * 1000),
      id: logentry.oid,
      message: logentry.commit.message,
      author: { name: logentry.commit.author.name, email: logentry.commit.author.email },
      parents: logentry.commit.parent
    }))
  };
  return result;
}

export async function describeGitRepo(dir: string, obsolete: boolean) {
  try {
    return await getRepoInfo(dir);
  } catch (e) {
    console.log("ERR", dir, e);
    return { status: "error" };
  }
}
