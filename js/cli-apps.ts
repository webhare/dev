import { backendConfig, closeServerSession, createServerSession, getServerSession } from '@webhare/services';
import { sleep, toCamelCase, toSnakeCase } from '@webhare/std';
import { runInWork } from '@webhare/whdb';
import { spawn } from "node:child_process";
import YAML from "yaml";


export async function runYamlScreen(code: string) {
  const res = YAML.parse(code);
  const sess = await runInWork(() => createServerSession("dev:runyamlscreen", { screen: toSnakeCase(res) }));

  const url = `${backendConfig.backendURL}?appmode=singleapp&app=dev:runyamlscreen(${sess})`;
  spawn("open", [url]);

  for (; ;) {
    const sessdata = await getServerSession("dev:runyamlscreen", sess);
    if (sessdata && "result" in sessdata) { //we received an answer
      await runInWork(() => closeServerSession(sess));
      return toCamelCase(sessdata.result);
    }
    await sleep(200);
  }
}
