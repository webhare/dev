/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

"use strict";

const acorn = require("acorn");
const injectAcornJsx = require('acorn-jsx/inject');
const injectAcornObjectRestSpread = require('acorn-object-rest-spread/inject');
injectAcornJsx(acorn);
injectAcornObjectRestSpread(acorn);

async function runAconParser(data) {
  const comments = [];

  const options =
  {
    onComment: comments,
    locations: data.options.locations || false,
    ecmaVersion: 9,
    plugins: { objectRestSpread: true, jsx: true },
    sourceType: "module"
  };

  if (data.options.sourcetype)
    options.sourceType = data.options.sourcetype;

  const tree = acorn.parse(data.data, options);

  const result =
  {
    tree: tree,
    comments: comments
  };

  return { success: true, parsed: result, error: "" };
}
module.exports = { runAconParser };
