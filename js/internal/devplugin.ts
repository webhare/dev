import { SiteResponse } from "@webhare/router/src/sitereponse";

interface DevPluginData {
  adddebugscript: boolean;
}

export function devSupportHook(hookdata: DevPluginData, composer: SiteResponse) {
  if (hookdata.adddebugscript)
    composer.insertAt("dependencies-bottom", `<script src="/.dev/debug.js" async></script>`);
  // IF(IsRequest()) ADDME for DYNAMIC requets only,.
  //   webdesign->InsertWithCallback(PTR this->PrintUsedResources(webdesign), "body-devbottom");
}
