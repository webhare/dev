import { WebHareBlob } from "@webhare/services/src/webhareblob";

declare module "@webhare/harescript/src/commonlibs" {
  interface CommonLibraries {
    "mod::dev/lib/internal/rewrite/rewrite.whlib": {
      rewriteFile(resourcename: string, input: WebHareBlob): Promise<{ success: boolean; result?: WebHareBlob }>;
    };
  }
}
