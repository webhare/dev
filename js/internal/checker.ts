import { CheckFunction, CheckResult, readRegistryKey } from "@webhare/services";

export async function verifyDevModule(): Promise<CheckResult[]> {
  const fails: CheckResult[] = [];
  const failpulldate = await readRegistryKey("dev.updatestate.lastfailedupdate") as Date | null;
  const failfixdate = await readRegistryKey("dev.updatestate.lastfailedfixmodules") as Date | null;

  if (failpulldate && failpulldate.getTime() > 0) {
    fails.push({
      messageText: "Unable to update the dev module - run wh dev:update manually",
      "type": "dev:cannotupdate",
      metadata: null,
      jumpTo: null,
      scopes: []
    });
  }
  if (failfixdate && failfixdate.getTime() > 0) {
    fails.push({
      messageText: "Unable to run fixmodules for the dev module - run wh dev:update manually",
      "type": "dev:cannotfix",
      metadata: null,
      jumpTo: null,
      scopes: []
    });
  }
  return fails;
}

export async function testLib() {
  // wh node -e 'require("@mod-dev/js/internal/checker.ts").testLib()'
  verifyDevModule().then(x => console.log(x));
}

verifyDevModule satisfies CheckFunction;
