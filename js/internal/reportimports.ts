// eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports -- can't use import in preload library
const fs = require("node:fs");

process.on("exit", () => {
  if (process.env.WEBHARE_DEV_REPORT_IMPORTS) {
    fs.writeFileSync(process.env.WEBHARE_DEV_REPORT_IMPORTS, JSON.stringify({
      imports: Object.keys(require.cache)
    }));
  }
});
