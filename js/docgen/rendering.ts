import { toFSPath } from "@webhare/services";
import * as TypeDoc from "typedoc";

import { program } from 'commander';
import { mkdir, rename } from "node:fs/promises";
import { join } from "node:path";
import { deleteRecursive } from "@webhare/system-tools/src/fs";

export async function renderDocsProject(app: TypeDoc.Application, project: TypeDoc.Models.ProjectReflection) {
  // Project may not have converted correctly
  const baseDocsDir = toFSPath("storage::dev/docgen");
  const outputDir = join(baseDocsDir, "docs.new");
  const finalDir = join(baseDocsDir, "docs");
  await mkdir(outputDir, { recursive: true });

  console.log("Generating doc update for", finalDir);

  if (project.children)
    for (const lib of project.children) {
      // Replace library names "forms/src/forms" with "@webhare/forms"
      const firstpart = lib.name.split('/')[0];
      lib.name = `@webhare/${firstpart}`;

      // get rid of 'defined in:' lines on the library page. noone cares @webhare/deps is defined on line 1 of webhare/deps
      lib.sources = [];
    }

  // Rendered docs
  await app.generateDocs(project, outputDir);
  // Alternatively generate JSON output. Doesn't seem that useful yet, we're not intending to rebuild the docs from scratch?
  if (program.opts().json)
    await app.generateJson(project, outputDir + "/documentation.json");

  await deleteRecursive(join(baseDocsDir, "docs"), { allowMissing: true });
  await rename(outputDir, finalDir);
}
