#!/bin/bash

function die()
{
  echo "$@"
  exit 1
}

[ -f "artifacts/dev.whmodule" ] || die "My dev.whmodule artifact isn't there"
[ -f "artifacts/buildinfo" ] || die "My buildinfo artifact isn't there"

source artifacts/buildinfo #adds #version
[[ $version =~ ^[0-9]+\.[0-9]+\. ]] || die "Unexpected version format: '${version}'"

MAJORMINOR="$(echo $version | cut -d. -f1,2)"
UPLOADURL="https://cms.webhare.dev/webdav/publisher/devmodule/dev-$MAJORMINOR.whmodule"

echo "Should upload to: $UPLOADURL"

# We do this check as late as possible to be able to debug the above. IF you want to test the last part, manually set WHBUILDSECRET_BUILDBOTPASSWORD during your CI run ?
[ -n "$WHBUILDSECRET_BUILDBOTPASSWORD" ] || die "Didn't receive the build bot password - cannot actually upload"
curl --include --fail --user "info+buildbot@webhare.nl:$WHBUILDSECRET_BUILDBOTPASSWORD" --upload-file artifacts/dev.whmodule "$UPLOADURL" || die "Upload failed"
exit 0
