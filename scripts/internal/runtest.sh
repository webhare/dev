#!/bin/bash

USEIMAGE="docker.io/webhare/platform:master"

if [[ "$CI_COMMIT_BRANCH" =~ ^release/.* ]]; then
  USEIMAGE="docker.io/webhare/platform:$(echo $CI_COMMIT_BRANCH | tr /. --)"
fi

LIMITTEST=""
if [ "$CI_BUILD_REF_NAME" == "autoupload" ]; then #Just testing the process - run only a single CI script for speed
  LIMITTEST=dev.test_support
fi

curl -s https://build.webhare.dev/ci/scripts/testmodule.sh | bash -s -- -w "$USEIMAGE" $LIMITTEST
