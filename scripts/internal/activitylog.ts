/*
   for now, start manually: wh run mod::dev/scripts/internal/activitylog.ts

   TODO: log activity to C++ code and builddocker.sh etc (watch WEBHARE_CHECKEDOUT_TO, not whtree)
         log activity to other modules, eg webhare_cue (can we follow all runkit linked modules ? watch ~/projects/ ?)

         we might need to build our own watchers for that, or even do that *anyway* and keep us running independently of webhare
         but we can't log() then either
         and is it really worth the trouble?
*/

import { backendConfig, log, subscribe, type BackendEvent } from "@webhare/services";

const lastchange = new Set<string>();
const checkinterval = 60_000;
let lastcleanup = new Date;

function getModule(file: string) {
  return file.startsWith("mod::") ? file.split("::")[1].split("/")[0]
    : file.startsWith(`direct::${backendConfig.installationroot}jssdk/`) ? "jssdk" : "";
}

function formatTime(time: Date) {
  return String(time.getHours()).padStart(2, "0") + ":" + String(time.getMinutes()).padStart(2, "0") + ":" + String(time.getSeconds()).padStart(2, "0");
}

function gotEvents(events: BackendEvent[]) {
  const changedfiles = new Set<string>(events.map(_ => (_ as unknown as { data: { resourcename: string } }).data.resourcename));

  for (const file of changedfiles) {
    const ignore = Boolean(file.match(/\.git/) || file.match(/platform\.generated/));
    // console.log({ ignore, file, module: getModule(file) });

    if (ignore)
      continue;

    if (!lastchange.has(file)) {
      lastchange.add(file);
      log("dev:activity", { type: "change", file, module: getModule(file) });
    }
  }
}

function cleanup() {
  const activeModules = Object.groupBy(lastchange, file => getModule(file) || "<other>");
  console.log(`${formatTime(lastcleanup)} - ${formatTime(new Date)}: ${Object.keys(activeModules).toSorted().join(", ") || "<slacking>"}`);

  const changedPerModule: Record<string, { files: number }> = Object.fromEntries(Object.entries(activeModules).map(([module, files]) => [module, { files: files?.length || 0 }]));
  log("dev:activity", { type: "change-overview", changedPerModule });

  lastcleanup = new Date;
  lastchange.clear();

  setTimeout(cleanup, checkinterval - (Date.now() % checkinterval)); //get to the next full minute
}

async function main() {
  await subscribe("system:modulefolder.*", gotEvents);
  setTimeout(cleanup, checkinterval - (Date.now() % checkinterval)); //get to the next full minute
}

main();
