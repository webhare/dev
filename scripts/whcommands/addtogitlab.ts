// short: Add current module to your Gitlab forge

import fs from 'node:fs/promises';
import * as git from 'isomorphic-git';
import { program } from 'commander';
import { devsupportSchema } from 'wh:wrd/dev';
import { Gitlab } from '@gitbeaker/rest';
import { executeGitCommand, executeGitCommandForeground } from '@mod-dev/lib/internal/modulemgmt/git';

program
  .name('addtogitlab')
  .option('-v --verbose', 'Verbose output')
  .parse();

const verbose: boolean = program.opts().verbose;

async function getForgeFor(namespace: string) {
  const allforges = await devsupportSchema.query("forge").select(["token", "url", "wrdId"]).execute();
  for (const forge of allforges) {
    if (!forge.token)
      continue;

    const gitlabapi = new Gitlab({ token: forge.token, host: forge.url });

    try {
      const group = await gitlabapi.Groups.show(namespace);
      return { forge: forge.url, full_path: group.full_path, namespaceId: group.id, gitlabapi };
    } catch (e) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      if (!((e as any)?.cause?.response?.status === 404)) {
        console.log("Could not connect to forge", forge.url, e);
        continue;
      }
    }

    //group not found
    try {
      const users = await gitlabapi.Users.all({ username: namespace });
      if (users.length === 1)
        return { forge: forge.url, full_path: users[0].name, namespaceId: users[0].namespace_id as number, gitlabapi };
    } catch (e) {
      console.log("Could not connect to forge", forge.url, e);
      continue;
    }
  }
  return null;
}

async function main() {
  let root;
  try {
    root = await git.findRoot({ fs, filepath: process.cwd() });
  } catch (e) {
    console.error(e);
    console.error("Could not find the root of the current git repository, if any");
    process.exit(1);
  }

  const modulename = root.split('/').at(-1);
  const namespace = root.split('/').at(-2);
  if (!modulename || !namespace) {
    console.error("Could not determine namespace or module name based on path " + root);
    process.exit(1);
  }

  if (verbose)
    console.log(`Inferred module name '${modulename}' and namespace '${namespace}' from path '${root}'`);

  const pathwithnamespace = `${namespace}/${modulename}`;
  const forge = await getForgeFor(namespace);
  if (!forge) {
    console.error(`Did not find a forge for namespace ${namespace} or we were unable to access it`);
    process.exit(1);
  }

  if (verbose)
    console.log(`Found forge ${forge.forge} with group '${forge.full_path}'`);

  let project;
  try {
    project = await forge.gitlabapi.Projects.show(pathwithnamespace);
  } catch (e) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    if (!((e as any)?.cause?.response?.status === 404))
      throw e;

    console.log(`Project ${pathwithnamespace} does not exist, creating`);
    //set up a usual WebHare mdoule project
    project = await forge.gitlabapi.Projects.create({
      path: modulename,
      namespaceId: forge.namespaceId,
      wikiAccessLevel: "disabled",
      snippetsAccessLevel: "disabled",
      buildsAccessLevel: "enabled",
      containerRegistryAccessLevel: "disabled"
    });
  }

  //Recreate origin
  await executeGitCommand(["remote", "remove", "origin"], { workingdir: root });
  await executeGitCommandForeground(["remote", "add", "-f", "origin", project.ssh_url_to_repo], { workingdir: root });
}

main();
