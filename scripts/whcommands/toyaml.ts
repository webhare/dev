import type * as Sp from "@mod-platform/generated/schema/siteprofile";
import * as devbridge from "@mod-platform/js/devsupport/devbridge";
import { nameToCamelCase } from "@webhare/std";
import { parseResourcePath } from "@webhare/services";
import { CSPMember, CSPMemberType } from "@webhare/whfs/src/siteprofiles";
import YAML from "yaml";

const YamlTypeMapping: { [type in CSPMemberType]: Sp.TypeMember["type"] } = {
  [CSPMemberType.String]: "string",
  [CSPMemberType.Integer]: "integer",
  [CSPMemberType.DateTime]: "datetime",
  [CSPMemberType.File]: "file",
  [CSPMemberType.Boolean]: "boolean",
  [CSPMemberType.Float]: "float",
  [CSPMemberType.Money]: "money",
  [CSPMemberType.WHFSRef]: "whfsref",
  [CSPMemberType.Array]: "array",
  [CSPMemberType.WHFSRefArray]: "whfsrefarray",
  [CSPMemberType.StringArray]: "stringarray",
  [CSPMemberType.RichDocument]: "richdocument",
  [CSPMemberType.IntExtLink]: "intextlink",
  [CSPMemberType.Instance]: "instance",
  [CSPMemberType.URL]: "url",
  [CSPMemberType.ComposedDocument]: "composeddocument",
  [CSPMemberType.Record]: "record",
  [CSPMemberType.FormCondition]: "formcondition" as Sp.TypeMember["type"],
  [CSPMemberType.HSON]: "hson",
  [CSPMemberType.Date]: "date",
  [CSPMemberType.Image]: "image",
};

function unresolveGid(module: string | null, ptr: string): string {
  if (module && ptr.startsWith(module + ":"))
    return ptr.substring(module.length + 1);
  return ptr;
}

function convertMembers(members: CSPMember[]): Sp.TypeMembers {
  const result: Sp.TypeMembers = {};
  for (const member of members) {
    const outmember: Sp.TypeMember = {
      type: YamlTypeMapping[member.type],
    };
    if (member.comment)
      outmember.comment = member.comment;
    if (member.children?.length)
      outmember.members = convertMembers(member.children);

    result[nameToCamelCase(member.name)] = outmember;
  }
  return result;
}

async function convertSiteProfile(toconvert: string): Promise<string> {
  const parsed = await devbridge.getParsedSiteProfile(toconvert);
  const module = parseResourcePath(toconvert)?.module || null;

  const result: Sp.SiteProfile = {
    gid: parsed.gid
  };

  if (parsed.gid)
    result.gid = unresolveGid(module, parsed.gid);
  if (parsed.contenttypes.length) {
    result.typeGroup = "FIXME - set this";
    console.log("# You'll need to setup a typeGroup: property");
    result.types = {};
    for (const [idx, type] of parsed.contenttypes.entries()) {
      //TODO need to come up with a name
      const outtype: Sp.Type = {
        namespace: type.namespace,
      };
      if (type.members?.length)
        outtype.members = convertMembers(type.members);

      result.types["type" + idx] = outtype;
    }
  }

  // console.error(parsed);
  return YAML.stringify(result);
}

async function main() {
  const toconvert = process.argv[2];
  if (!toconvert)
    throw new Error(`No file to convert to yaml provided`);

  console.log(await convertSiteProfile(toconvert));
}

main();
