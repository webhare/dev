#!/bin/bash
source $WEBHARE_DIR/lib/wh-functions.sh
source $WEBHARE_DATAROOT/.webhare-envsettings.sh
export WEBHARE_INEDITOR=1

# __sublimerun compensates for the 'lingering process' bug in sublime
# and it also allows us a chance to implement 'default' arguments for scripts

if [ -f "$HOME/.webhare-sublime-currentpid" ]; then
  CURRENTPID=`cat $HOME/.webhare-sublime-currentpid`
  if [ "`ps -o lstart= $CURRENTPID`" == "`cat $HOME/.webhare-sublime-currentpidlstart`" ]; then
    kill -9 $CURRENTPID
    echo "Killed lingering script with pid $CURRENTPID"
  fi
fi

SCRIPT="$1"
shift

if [ -z "$__DID_AUTO_ARGUMENTS" -a -f "${SCRIPT%.*}.args" ]; then
  export __DID_AUTO_ARGUMENTS=1
  # reexec with our command line so hopefully they get reprocessed?
  ARGS="$(egrep '^[^#].+' "${SCRIPT%.*}.args")"
  exec /bin/bash -c "$BASH_SOURCE $SCRIPT $@ $ARGS"
  exit 255
fi

# Record current pid so we can kill it if needed
echo $$ > "$HOME/.webhare-sublime-currentpid"
ps -o lstart= $$ > "$HOME/.webhare-sublime-currentpidlstart"

if [ "${SCRIPT: -3}" == ".es" ] || [ "${SCRIPT: -3}" == ".js" ] || [ "${SCRIPT: -3}" == ".ts" ]; then
  exec_wh_runjs "$SCRIPT" "$@"
else
  exec_wh_runwhscr "$SCRIPT" "$@"
fi

exit 1
