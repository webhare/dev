import { elements, getAttr } from '@mod-system/js/internal/generation/xmlhelpers'; //TODO safer to copy?
import { loadlib } from '@webhare/harescript';
import { ModDefYML } from '@webhare/services/src/moduledefparser';
import { DOMParser, XMLSerializer } from '@xmldom/xmldom';
import { program } from 'commander';
import { readFile } from 'fs/promises';
import { stringify } from 'yaml';
import { resolve } from 'path';
import { WebHareBlob, toResourcePath } from '@webhare/services';
import { storeDiskFile } from '@webhare/system-tools/src/fs';

program
  .option("--extract", "Remove generated nodes from the source file")
  .argument("<input>", "Input file")
  .parse();

const extract: boolean = program.opts().extract;

async function main() {
  const inputfilename = resolve(program.args[0]);
  if (!inputfilename.endsWith("moduledefinition.xml"))
    throw new Error(`Only moduledefinition.xml files are supported, got ${inputfilename}`);

  const validateres = await loadlib("mod::system/lib/validation.whlib").validateSingleFile(toResourcePath(inputfilename));
  if (validateres.errors.length)
    throw new Error(`Validation failed: ${validateres.errors.join(", ")}`);

  const inputfile = await readFile(inputfilename, "utf8");
  const modXml = new DOMParser().parseFromString(inputfile, "text/xml");

  const result: Partial<ModDefYML> = {};
  const publishernode = modXml.getElementsByTagNameNS("http://www.webhare.net/xmlns/system/moduledefinition", "publisher").item(0);
  if (publishernode) {
    for (const node of elements(publishernode.getElementsByTagNameNS(publishernode.namespaceURI, "webfeature"))) {
      //TODO rewrite applicability
      const webfeature: NonNullable<ModDefYML["webFeatures"]>[string] = {};
      if (node.hasAttribute("title"))
        webfeature.title = node.getAttribute("title")!;
      if (node.hasAttribute("siteprofile"))
        webfeature.siteProfile = node.getAttribute("siteprofile")!;

      if (getAttr(node, "hidden", false))
        webfeature.hidden = true;

      if (node.hasAttribute("name")) {
        result.webFeatures ||= {};
        result.webFeatures[node.getAttribute("name")!] = webfeature;
        publishernode.removeChild(node);
      }
    }
  }

  if (Object.keys(result).length) { //things were converted
    if (extract) {
      const rawxml = new XMLSerializer().serializeToString(modXml);
      const final = (await loadlib("mod::dev/lib/internal/rewrite/rewrite.whlib").rewriteFile(toResourcePath(inputfilename), WebHareBlob.from(rawxml)));
      if (!final.success)
        throw new Error(`rewrite of ${inputfilename} failed`);

      await storeDiskFile(inputfilename, await final.result!.text(), { overwrite: true });
    }
  }
  console.log(stringify(result));
}

main();
