import { devsupportSchema } from "wh:wrd/dev";
import { executeGitCommand, executeGitCommandForeground, getRepoInfo } from '@mod-dev/lib/internal/modulemgmt/git';
import * as git from 'isomorphic-git';
import fs from 'node:fs/promises';
import process from 'node:process';
import { Gitlab, MergeRequestSchema, PipelineSchema } from '@gitbeaker/rest';
import { pick, sleep } from '@webhare/std';
import { logDebug } from '@webhare/services';
import { beginWork, commitWork } from "@webhare/whdb/src/whdb";
import { program } from 'commander';

program
  .name('automerge')
  .option('-v --verbose', 'Verbose output')
  .parse();

const verbose: boolean = program.opts().verbose;

function shortLogMessage(text: string) {
  text = text.split('\n')[0];
  if (text.length > 50)
    text = text.substr(0, 50) + '…';
  return text;
}

async function getAutomergeConfig(forrepo: string) {
  const user = process.env.USER;
  if (!user || ["root", "git", "webhare"].includes(user)) {
    console.error(`Could not determine user name`);
    process.exit(1);
  }

  let apiroot = '', project = '';
  const git_repo = forrepo.match(/^git@([^:]*):(.*)\.git$/)
    || forrepo.match(/^git@([^:]*):(.*)$/); //URLs may or may not end in .git, at least on GitLab
  if (git_repo) {
    apiroot = `https://${git_repo[1]}`;
    project = git_repo[2];
  } else {
    console.error(`Cannot figure out API root for git url: ${forrepo}`);
    process.exit(1);
  }

  //Look up this forge
  let forgeid = await devsupportSchema.search("forge", "url", apiroot);
  if (!forgeid) {
    console.log(`No forge found for ${apiroot} - creating`);
    await beginWork();
    forgeid = await devsupportSchema.insert("forge", ({ url: apiroot }));
    await commitWork();
  }

  const forgesettings = await devsupportSchema.getFields("forge", forgeid, ["token"]);
  if (!forgesettings?.token) {
    console.error(`Set up an API token for forge ${apiroot} in the dev deploy app. https://my.webhare.dev/?app=dev:deploy/forges`);
    process.exit(1);
  }
  return { user, apiroot, token: forgesettings.token, project };
}

async function main() {
  let root;
  try {
    root = await git.findRoot({ fs, filepath: process.cwd() });
  } catch (e) {
    console.error("Could not find the root of the current git repository, if any");
    process.exit(1);
  }

  let repoinfo = await getRepoInfo(root);
  if (repoinfo.branch.startsWith("automerge/")) {
    console.error(`You are on automerge branch '${repoinfo.branch}', you probably want to select 'master' or 'main'`);
    process.exit(1);
  }

  //Ensure things are up-to-date. automerge.whscr did quite a bit of work but I'm not sure why we can't just have git do it..
  //TODO a bit less noise if we fetch first and then see if we need to rebase. pull combines fetch&rebase
  const exitcode = await executeGitCommandForeground(["pull", "--rebase=merges", "--quiet"], { workingdir: root });
  if (exitcode !== 0) {
    console.error(`Could not pull from remote repository: ${exitcode}`);
    process.exit(1);
  }

  repoinfo = await getRepoInfo(root); //up-to-date
  if (repoinfo.head_oid === repoinfo.origin_oid) {
    console.log(`Current commit (${shortLogMessage(repoinfo.commits[0]?.message)}) has already been merged`);
    process.exit(0);
  }

  const newcommits = [];
  for (const commit of repoinfo.commits) {
    if (commit.id === repoinfo.origin_oid)
      break;
    newcommits.push(commit);
  }

  if (verbose)
    console.log(newcommits);

  const config = await getAutomergeConfig(repoinfo.remote_url);
  const pushbranch = `automerge/${config.user}-${repoinfo.branch.replaceAll("/", "-")}`;

  const remote_pushbranch_res = await executeGitCommand(["ls-remote", "--quiet", "origin", pushbranch], { workingdir: root });
  const remote_pushbranch_sha = remote_pushbranch_res.output.split('\t')[0];
  if (remote_pushbranch_sha !== repoinfo.head_oid) {
    const branchres = await executeGitCommandForeground(["branch", "-f", pushbranch, repoinfo.branch], { workingdir: root });
    if (branchres !== 0) {
      console.error(`Could not create branch ${pushbranch}: ${branchres}`);
      process.exit(1);
    }

    const pushoptions = ["-u", "--force"];
    if (!verbose)
      pushoptions.push("--quiet");

    const pushres = await executeGitCommandForeground(["push", ...pushoptions, "origin", pushbranch], { workingdir: root });
    if (pushres !== 0) {
      console.error(`Could not push branch ${pushbranch}: ${pushres}`);
      process.exit(1);
    }
  }

  const gitlabclient = new Gitlab({ host: config.apiroot, token: config.token });
  if (verbose)
    console.log(`Looking up pending merge request`);

  let self;
  try {
    self = await gitlabclient.Users.showCurrentUser();
  } catch (e) {
    console.error(`Could not look up current user. Token expired?: ${e}`);
    console.error(`You can set up a new API token for forge ${config.apiroot} in the dev deploy app. https://my.webhare.dev/?app=dev:deploy/forges`);
    process.exit(1);
  }

  let mergerequest: MergeRequestSchema = (await gitlabclient.MergeRequests.all({
    projectId: config.project,
    state: 'opened',
    scope: 'created_by_me',
    sourceBranch: pushbranch,
    targetBranch: repoinfo.branch
  }))[0];

  if (!mergerequest) {
    console.log("Need to create a new merge request");

    let title = '';
    if (newcommits.length > 1)
      title = `${newcommits.length} commits: `;
    title += newcommits.map(c => shortLogMessage(c.message)).join(', ');
    title = title.substring(0, 80);

    mergerequest = await gitlabclient.MergeRequests.create(config.project, pushbranch, repoinfo.branch, title, {
      removeSourceBranch: true,
      assigneeId: self.id,
    });
    console.log(`New merge request !${mergerequest.iid} created - ${mergerequest.web_url} with status: ${mergerequest.merge_status}`);
  } else {
    console.log(`Have merge request !${mergerequest.iid}: - ${mergerequest.web_url}, status: ${mergerequest.merge_status}`);
  }

  //Check if the merge request contains a .gitlab-ci.yaml
  let hasCI = false;
  try {
    await gitlabclient.RepositoryFiles.show(config.project, ".gitlab-ci.yml", mergerequest.sha);
    hasCI = true;
  } catch (ignore) {
    //hasCI will remain false
    if (verbose)
      console.log(`No .gitlab-ci.yml found in merge request, not waiting for CI`);
  }

  if (hasCI) {
    //Wait for a pipeline to appear
    const waituntil = Date.now() + 10000; // Wait max 10 seconds for the pipeline to appear
    let pipeline: PipelineSchema | null = null;
    while (Date.now() < waituntil) {
      /* we're running into issues where MergeRequests.accept gives a 405 Method not allowed.,
        see if we need to wait for pipelines to move out of 'created' state first ...
        */
      const pipelines = await gitlabclient.Pipelines.all(config.project, { ref: pushbranch });
      pipeline = pipelines.find(p => p.sha === repoinfo.head_oid && p.status !== "created") || null;
      if (pipeline)
        break;
      await sleep(250);
      console.log(`Waiting for pipeline to appear...`); //TODO only every second
    }

    if (!pipeline) {
      console.error(`Pipeline didn't appear - check ${mergerequest.web_url}`);
      process.exit(1);
    }
    if (pipeline.status === "failed") {
      console.error(`CI run for this commit has failed - check ${mergerequest.web_url}`);
      process.exit(1);
    }
    console.log("Pipeline status: " + pipeline.status);
  }

  //Wait for merge request to go out of 'checking' state
  for (; ;) {
    mergerequest = await gitlabclient.MergeRequests.show(config.project, mergerequest.iid);
    logDebug("dev:automerge", { mergerequest });
    console.log(`MR status: ${mergerequest.merge_status}, detailed: ${mergerequest.detailed_merge_status}`);
    if (!(mergerequest.merge_status === "checking" || mergerequest.merge_status === "unchecked"))
      break;

    await sleep(250);
    mergerequest = await gitlabclient.MergeRequests.show(config.project, mergerequest.iid);
  }

  //Log merge request status to see if we can figure out why accept gets entity errors
  if (verbose)
    console.log(pick(mergerequest, ["merge_status", "detailed_merge_status"]));

  if (hasCI) {
    await gitlabclient.MergeRequests.accept(config.project, mergerequest.iid, { mergeWhenPipelineSucceeds: true });
  } else {
    await gitlabclient.MergeRequests.accept(config.project, mergerequest.iid);
  }

  process.exit(0); //TODO without this we hang after the first gitlab request. not sure why
}

main();
