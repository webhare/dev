#!/bin/bash

[ -n "$WEBHARE_CHECKEDOUT_TO" ] || due "WEBHARE_CHECKEDOUT_TO not set"
cd "$WEBHARE_CHECKEDOUT_TO" || exit 1

if [ -n "$(git status -s)" ]; then
  git status -s
  echo "Your tree is unclean. Stash or commit changes first!"
  exit 1
fi

cd whtree/modules || exit 1
for MOD in *; do
  if [ "$MOD" != "webhare_testsuite" ]; then
    wh dev:cleanupmodule --loadlibs --xml "$MOD"
  fi
done
