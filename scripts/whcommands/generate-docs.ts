/*
watch mode: wh dev:generate-docs -w

*/

import * as TypeDoc from "typedoc";

import { program } from 'commander';
import { uploadGeneratedDocumentation } from "@mod-dev/js/docgen/upload";
import { renderDocsProject } from "@mod-dev/js/docgen/rendering";
import { setupDocGenerator } from "@mod-dev/js/docgen/generate";

program
  .name('automerge')
  .option('-w --watch', 'Watch docs')
  .option('--json', 'Add JSON data')
  .option('--upload', 'Upload after generating')
  .parse();

async function renderProject(app: TypeDoc.Application, project: TypeDoc.Models.ProjectReflection) {
  await renderDocsProject(app, project);
  await uploadGeneratedDocumentation();
}

async function main() {
  const app = await setupDocGenerator();

  // TODO can we get typedoc to use webhare's typescript instead of a builtin one? (nice to have but prevents version mismatches)
  console.log(`Using TypeScript ${app.getTypeScriptVersion()} in ${app.getTypeScriptPath()}`);

  // app.renderer.hooks.

  // app.options.fixCompilerOptions({ noImplicitAny: true });

  // app.options._compilerOptions.noImplictAny = false;
  if (program.opts().watch) {
    await app.convertAndWatch(p => renderProject(app, p));
  } else {
    const project = await app.convert();
    if (project)
      await renderProject(app, project);
    else
      process.exit(1);
  }
}

main().catch(console.error);
