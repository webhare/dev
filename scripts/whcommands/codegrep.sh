#!/bin/bash

# A drop-in replacement for the HareScript-based codegrep based on ripgrep, with a few changes:
# - Searches case insensitive by default, use the -I option to make it case sensitive
# - Only searches through modules on disk, doesn't search the Publisher repository
# - Grep-like output (result lines are grouped with a file path header and only prefixed with line numbers)
# - The results are now formatted by ripgrep; to show the output in a format compatible with the HareScript codegrep, set the
#   --format parameter to 'codegrep' (custom formats aren't supported)
# - No support for the --skippath, --scanpath, --prefix, --moduleprefix and --stdmoduleprefix options
# The Harescrip codegrep can still be used through 'wh run mod::dev/scripts/tools/codegrep.whscr'

# Check if ripgrep is available
if ! which rg >/dev/null 2>&1; then
  echo "ripgrep is not installed, run brew install ripgrep"
  exit 1
fi

# Show usage and exit
exit_usage()
{
  echo "Usage: codegrep [-I] [--context <numlines>] [--nocolor] [--format ripgrep|codegrep] [--css] [--harescript] [--html] [--js] [--witty] [--xml] [--sort <path|modified|accessed|created>] <pattern>"
  exit 1
}

# Read arguments
rg_args=()
pattern=$1
matchcase=
format=
while (( $# > 0 )); do
  if [ "$pattern" == "-I" ]; then
    matchcase=1
  elif [ "$pattern" == "--context" ]; then
    shift
    pattern="$1"
    if ! [[ "$pattern" =~ ^[0-9]+$ ]]; then
      exit_usage
    fi
    rg_args+=("--context=$pattern")
  elif [ "$pattern" == "--format" ]; then
    shift
    format="$1"
  elif [ "$pattern" == "--sort" ]; then
    shift
    rg_args+=(--sort="$1")
  elif [ "$pattern" == "--nocolor" ]; then
    rg_args+=(--color=never)
  elif [ "$pattern" == "--js" ]; then
    rg_args+=(--glob '*.{js,jsx,es,esx,ts,tsx}')
  elif [ "$pattern" == "--css" ]; then
    rg_args+=(--glob '*.{css,less,scss}')
  elif [ "$pattern" == "--xml" ]; then
    rg_args+=(--glob '*.{siteprl,xml,wsdl,xsd,webfields}')
  elif [ "$pattern" == "--harescript" ]; then
    rg_args+=(--glob '*.{whlib,whscr,shtml}')
  elif [ "$pattern" == "--html" ]; then
    rg_args+=(--glob '*.{html,shtml}')
  elif [ "$pattern" == "--witty" ]; then
    rg_args+=(--glob '*.{witty}')
  else
    #TODO: Error on invalid switch? What about searching for something starting with "-"?
    break
  fi
  shift
  pattern="$1"
done

# If -I wasn't supplied, do a case insensitive search
if [ -z "$matchcase" ]; then
  rg_args+=(--ignore-case)
fi

# If --format was set to 'codegrep', change the output
if [ "$format" == "codegrep" ]; then
  rg_args+=(--column --with-filename --no-heading --field-context-separator=: --no-context-separator)
elif [ -n "$format" ] && [ "$format" != "ripgrep" ]; then
  exit_usage
fi

# We need a pattern to search for
if [ -z "$pattern" ]; then
  exit_usage
fi

# Find the directories containing modules and add the JavaScript SDK
rootmoduledir=$(wh getrootdir)/whtree/modules
installedmoduledir=$(wh getdatadir)/installedmodules
jssdkdir=$(wh getrootdir)/whtree/jssdk

# Do the actual search
rg --engine=auto "${rg_args[@]}" --regexp "$pattern" "$rootmoduledir"/* "$installedmoduledir"/* "$jssdkdir"/*
