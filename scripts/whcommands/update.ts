import { executeGitCommand } from "@mod-dev/lib/internal/modulemgmt/git";
import { loadlib } from "@webhare/harescript";
import { defaultDateTime } from "@webhare/hscompat";
import { buildGeneratorContext } from "@mod-system/js/internal/generation/generator";
import { applyConfiguration, backendConfig, isWebHareRunning, resolveResource, toFSPath } from "@webhare/services";
import { elements } from "@mod-system/js/internal/generation/xmlhelpers";
import { beginWork, commitWork } from "@webhare/whdb";
import { spawn } from "node:child_process";
import { encodeString } from "@webhare/std";
import { storeDiskFile } from "@webhare/system-tools";
import { mkdirSync, existsSync } from "node:fs";

async function updateXMLCatalog() {
  //vscode xml extension can use this

  //Generate a new XSD catalog
  const schemas = new Array<{ namespace: string; uri: string }>;

  //Built-in locations. These schemas can't be enumerated from the modXML files (yet? should probably move to YML while we're at it)
  for (const builtin of [
    { namespace: "http://www.webhare.net/xmlns/publisher/siteprofile", xmlschema: "mod::publisher/data/siteprofile.xsd" },
    { namespace: "http://www.webhare.net/xmlns/publisher/forms", xmlschema: "mod::publisher/data/forms/formdef.xsd" },
    { namespace: "http://www.webhare.net/xmlns/publisher/forms/appinfo", xmlschema: "mod::publisher/data/forms/appinfo.xsd" },
    { namespace: "http://www.webhare.net/xmlns/system/moduledefinition", xmlschema: "mod::system/data/moduledefinition.xsd" },
    { namespace: "http://www.webhare.net/xmlns/wrd", xmlschema: "mod::wrd/data/siteprofile.xsd" },
    { namespace: "http://www.webhare.net/xmlns/system/common", xmlschema: "mod::system/data/common.xsd" },
    { namespace: "http://www.webhare.net/xmlns/tollium/common", xmlschema: "mod::tollium/data/common.xsd" },
    { namespace: "http://www.webhare.net/xmlns/tollium/appinfo", xmlschema: "mod::tollium/data/appinfo.xsd" },
    { namespace: "http://www.webhare.net/xmlns/whdb/databaseschema", xmlschema: "mod::system/data/validation/databaseschema.xsd" },
    { namespace: "http://www.webhare.net/xmlns/wrd/schemadefinition", xmlschema: "mod::wrd/data/schemadefinition.xsd" },
    { namespace: "http://www.webhare.net/xmlns/system/testinfo", xmlschema: "mod::system/data/testinfo.xsd" },
  ])
    schemas.push({ namespace: builtin.namespace, uri: toFSPath(builtin.xmlschema) });

  //FIXME devbridge should offer this or webhare should just parse the XSD locations for us
  const context = await buildGeneratorContext(null, false);
  for (const mod of context.moduledefs) {
    if (mod.modXml) {
      for (const comp of elements(mod.modXml.getElementsByTagNameNS("http://www.webhare.net/xmlns/system/moduledefinition", "components"))) {
        const namespace = comp.getAttribute("namespace");
        const xmlschema = comp.getAttribute("xmlschema");
        if (!namespace || !xmlschema)
          continue;

        for (const uri of [
          toFSPath(resolveResource(mod.resourceBase, xmlschema)),
          toFSPath(resolveResource(mod.resourceBase, "data/" + xmlschema))
        ])
          if (existsSync(uri)) {
            schemas.push({ namespace, uri });
            break;
          }
      }
    }
  }

  schemas.sort((lhs, rhs) => lhs.namespace.localeCompare(rhs.namespace));

  const catalog = `<catalog xmlns="urn:oasis:names:tc:entity:xmlns:xml:catalog">
  ${schemas.map(schema => `<uri name="${encodeString(schema.namespace, 'html')}" uri="${encodeString(schema.uri, 'html')}" />`).join('\n  ')}
</catalog>`;

  mkdirSync(toFSPath("storage::dev"), { recursive: true });
  storeDiskFile(toFSPath("storage::dev/catalog.xml"), catalog, { overwrite: true });
}

async function main() {
  const result = await executeGitCommand(["pull"], { workingdir: backendConfig.module.dev.root });
  console.log(result.output.trim());

  if (await isWebHareRunning()) {
    await beginWork();
    await loadlib("mod::system/lib/configure.whlib").writeRegistryKey("dev.updatestate.lastpullresult", result);
    await loadlib("mod::system/lib/configure.whlib").writeRegistryKey("dev.updatestate.lastfailedupdate", result.exitcode !== 0 ? new Date() : defaultDateTime);
    await commitWork();
  }
  if (result.exitcode !== 0) {
    console.error(result);
    process.exit(1);
  }

  if (await isWebHareRunning())
    await applyConfiguration({ subsystems: ["all"], source: "dev:update", modules: ["dev"] });

  await updateXMLCatalog();

  //Run fixmodules
  if (await isWebHareRunning()) { //fixmodules requires assetpack control to compileafter updating..
    const proc = spawn(backendConfig.installationroot + "/bin/wh", ["fixmodules", "dev"], {
      stdio: ["inherit", "inherit", "inherit"]
    });

    const fixexitcode = await new Promise<number | string>(resolve =>
      proc.on("exit", (code: number, signal: string) => resolve(signal || code)));

    if (await isWebHareRunning()) {
      await beginWork();
      await loadlib("mod::system/lib/configure.whlib").writeRegistryKey("dev.updatestate.lastfailedfixmodules", fixexitcode !== 0 ? new Date() : defaultDateTime);
      await commitWork();
    }

    if (fixexitcode !== 0)
      console.error("Fixmodules failed", fixexitcode);
  }
}

main();
