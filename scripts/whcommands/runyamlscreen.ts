/* Examples

YOURNAME=$(wh dev:runyamlscreen '
title: Identify yourself
footerButtons: [ok, cancel]
bodyLines:
  - textedit:
      name: firstName
      title: Your name please
' | jq .firstName)

echo $YOURNAME

*/

import { runYamlScreen } from '@mod-dev/js/cli-apps';
import { program } from 'commander';

program
  .argument("<screen>", "Screen to run")
  .parse();

async function main() {
  console.log(JSON.stringify(await runYamlScreen(program.args[0])));
}

main();
